# nginx auth a2aclr module

Features:

* Integrate ARPA2 Resource ACLs into nginx
* Use a central policy database for authorizing requests

## Requirements

* source of liba2aclr 0.7.1 (ARPA2 Resource ACLs, part of [libarpa2common])
* source of [nginx] >=1.17

## Compile nginx with this module

Clone [libarpa2common], this repository and fetch the [nginx] source.

```sh
$ git clone https://gitlab.com/arpa2/libarpa2common.git
$ git clone https://gitlab.com/arpa2/nginx-auth-a2aclr.git
$ curl -O https://nginx.org/download/nginx-1.19.1.tar.gz
$ sha256 nginx-1.19.1.tar.gz
SHA256 (nginx-1.19.1.tar.gz) = a004776c64ed3c5c7bc9b6116ba99efab3265e6b81d49a57ca4471ff90655492
$ tar zxf nginx-1.19.1.tar.gz
$ _liba2common="$(pwd)/libarpa2common"
$ _ngxa2aclr="$(pwd)/nginx-auth-a2aclr"
$ cd nginx-1.19.1
$ ./configure --with-compat \
    --with-cc-opt="-O0 -I${_liba2common}/src -I${_ngxa2aclr}" \
    --with-ld-opt="-L/usr/local/lib" \
    --add-dynamic-module="${_ngxa2aclr}"
$ make
$ sudo make install
```

The next step is to create an nginx configuration and then run
nginx. See the [Example](#example) section for a working example.

## Configuration directives

The following four directives are recognized by this module:

### auth_a2aclr_db
**contexts: main, server**

The path to an A2ACLR policy database. I.e.
`/etc/nginx/conf/a2aclr-policy.db`. The [syntax of a policy database],
as well as each [possible permission] are documented in [a2aclr(3)].

### auth_a2aclr_realm
**contexts: main, server, location**

The realm to use for a lookup. This parameter supports [nginx
variables] and named captures.

### auth_a2aclr_class
**contexts: main, server, location**

The resource class to use for a lookup. This parameter supports
[nginx variables] and named captures.

### auth_a2aclr_instance
**contexts: main, server, location**

The resource instance to use for a lookup. This parameter supports
[nginx variables] and named captures.

## Example

What follows is an example of a web application to manage the stock
of truck tyres. It has the imaginative name *Truck Tyre Stock* and
is running on the ficticious domain *trucktyres.example*.  This
application is used by a service provider named "Worldmaster" and
a tyre manufacturer named "Fineyear". Tyres enter stock whenever
Fineyear produced new tyres, and leave stock whenever Worldmaster
has mounted some tyres on a truck. The main two entities of this
system are *tyres* and *mountings*.

The web application offers a classic REST API with a standard CRUD
interface (create, read, update, delete) so that tyres and mountings
can be easily created, modified and deleted. The two main urls used
in this system are *http://trucktyres.example/api/tyres* and
*http://trucktyres.example/api/mountings*.

First we define an access right policy. We let anyone from Fineyear
create new tyres. Any employee from Worldmaster may read and update
tyres and will delete them once they're mounted on a truck.
Furthermore, anyone at Worldmaster may create, read and update
mountings, but only admin@worldmaster.example may delete a mounting.
We store this policy in a text file in **/etc/nginx/conf/tts-policy.db**:

```
# realm			selector		rights	resource				instance
trucktyres.example	@worldmaster.example	RWD	Truck-Tyre-Stock-XXXXXXXXXXXXXXXXXXX	/api/tyres
trucktyres.example	@worldmaster.example	CRW	Truck-Tyre-Stock-XXXXXXXXXXXXXXXXXXX	/api/mountings
trucktyres.example	admin@worldmaster.example	CRWD	Truck-Tyre-Stock-XXXXXXXXXXXXXXXXXXX	/api/mountings
trucktyres.example	@fineyear.example	C	Truck-Tyre-Stock-XXXXXXXXXXXXXXXXXXX	/api/tyres
```

Note that *C*, *R*, *W* and *D* map to create, read, update and
delete, respectively. *Truck-Tyre-Stock-XXXXXXXXXXXXXXXXXXX* is a
pseudo UUID picked for this web application but can be any randomly
generated UUID. Each field is separated by a TAB character.  The
exact syntax of a policy file, as well as all possible access rights
are documented in [a2aclr(3)].

Next we show an nginx configuration file that applies this policy
on the aforementioned urls.

```c
http {
	auth_a2aclr_db "/etc/nginx/conf/tts-policy.db";

	server {
		listen       80;
		server_name  trucktyres.example;
		root         /var/www/htdocs;

		auth_a2aclr_realm $server_name;

		# UUID for the application we're serving, this can be any
		# randomly generated (pseudo) UUID.
		auth_a2aclr_class "Truck-Tyre-Stock-XXXXXXXXXXXXXXXXXXX";

		location /api/tyres {
			auth_a2aclr_instance /api/tyres;
		}

		location /api/mountings {
			auth_a2aclr_instance /api/mountings;
		}
	}
}
```

We now start nginx with this configuration and fire some requests
at it. If nginx is [compiled and installed](#compile-nginx-with-this-module) and the above config is located in
**/etc/nginx.conf** it can be started with `nginx -c /etc/nginx.conf`.

First we impersonate as Sean, an employee of Fineyear, and try to
put a new tyre in stock that has just rolled out of the factory:

```sh
$ curl \
    -u sean@fineyear.example:supersecretpass \
    -H 'Content-Type: application/json' \
    -X POST \
    -d '@-' \
    http://trucktyres.example/api/tyres <<'EOF'
{
  "serial_number":   "6012021194",
  "service_provider": "Worldmaster",
  "brand": "Fineyear",
  "size":  "385/65R22.5",
  "type":  "KMAX T"
}
EOF
```

Given that sean@fineyear.example can be authenticated via basic auth,
the above request should succeed. The nginx log contains the details:

```
looking up trucktyres.example C in /etc/nginx/conf/tts-policy.db, client: XXX, server: trucktyres.example, request: "POST /api/tyres HTTP/1.1", host: "XXX"
W: PERMITTED, client: XXX, server: trucktyres.example, request: "POST /api/tyres HTTP/1.1", host: "XXX"
```

### Advanced patterns

A neat trick that has nothing to do with this module but all with
the capabilities of the PCRE library that is used by nginx when
found on the system nginx was compiled, are named captures. This
is a way to name parts of a string that matches a regular expression.
For example consider the following snippet from an nginx config
file:

```c
		location ~ (?<p>.*) {
			auth_a2aclr_instance $p;
		}
```

Here a named capture "p" is used to map any path of a url to a
corresponding instance in the policy file. nginx must be compiled
with perl compatible regular expression support in order to support
named captures. This feature can be used with all directives that
can take an nginx variable.

[nginx]: https://nginx.org
[libarpa2common]: https://gitlab.com/arpa2/libarpa2common
[a2aclr(3)]: https://netsend.nl/arpa2/a2aclr.3.html
[syntax of a policy database]: https://netsend.nl/arpa2/a2aclr.3.html#RESOURCE_POLICY_IMPORT_FILE_FORMAT
[possible permission]: https://netsend.nl/arpa2/a2aclr.3.html#DEFINED_RIGHTS
[nginx variables]: https://nginx.org/en/docs/varindex.html
[uuid]: http://uuid.arpa2.org
